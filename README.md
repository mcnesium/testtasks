test project to get the following running:

* a python application
* in a docker container
* build and debug using vscode

WIP: when starting it in vscodes "run and debug" tab (F5), it always takes two attempts to actually get it running. I assume the container is not ready yet when vscode tries to attach to it. still investigating, how I can fix this. any help very much welcome!
